require 'helper'

class TestExperianOnDemand < Test::Unit::TestCase
  context "parse xml addresses" do
    setup do
      @experian = ExperianOnDemand.new("username", "password")
    end

    should "convert good results into addresses - (search term normal)" do
      @experian.xml_result = File.open("test/fixtures/success_result_1.xml")
      assert addresses = @experian.get_addresses
      assert_equal 8, addresses.size
      assert_equal "16 Brisbane Street, ALBION  VIC  3020", addresses.first[:partial_address]
      assert_equal "AUS|632261|05OAUSHgHcBwAAAAABAwEAAAAA3I3Q0YAhMAYAAAAAAAAAMTYAPAAAAAD.....AAAAAAAAAAAAMTYgVmljdG9yaWEgU3QsIEJyaXNiYW5lAA--", addresses.first[:moniker]
      assert_equal "16 Brisbane Street, ALBION  VIC", addresses.first[:picklist]
      assert_equal "3020", addresses.first[:postcode]
      assert_equal 60, addresses.first[:score]
    end

    should "handle nil result e.g XXXXXXX" do
      @experian.xml_result = File.open("test/fixtures/success_but_no_results.xml")
      assert addresses = @experian.get_addresses
      assert_equal 1, addresses.size
      assert_equal "No matches", addresses.first[:picklist]
    end

    should "handle no too many results e.g West End, QLD" do
      @experian.xml_result = File.open("test/fixtures/success_too_many_results.xml")
      assert addresses = @experian.get_addresses
      assert_equal 1, addresses.size
      assert_equal "Search cancelled (too many matches)", addresses.first[:picklist]
    end

    should "handle server error e.g 500" do
      @experian.xml_result = File.open("test/fixtures/server_error.xml")
      assert addresses = @experian.get_addresses
      assert_equal 0, addresses.size
    end
  end

  context "handle network weirdness" do
    setup do
      @experian = ExperianOnDemand.new("username", "password")
    end

    should " accept fakeweb responses gracefully" do
      3.times do
        assert @experian.search('blah blah')
        assert_equal @experian.address_list, []
        assert_equal @experian.error, nil
      end
    end
  end
end
