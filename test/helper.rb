require 'rubygems'
require 'bundler'
begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  $stderr.puts e.message
  $stderr.puts "Run `bundle install` to install missing gems"
  exit e.status_code
end

require 'test/unit'
require 'shoulda'
require 'fakeweb'

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))
require 'experian_on_demand'

FakeWeb.allow_net_connect = false
FakeWeb.register_uri(:post, "https://ws2.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx",
                       [{:response => '', :status => ["200", "OK"]},
                        {:response => '',  :status => ["404", "Not Found"]},
                        {:response => '',  :status => ["500", "Not Found"]}])

class Test::Unit::TestCase
end
