require 'faraday'
require 'nokogiri'

class ExperianOnDemand
  attr_accessor :address, :address_list, :xml_result, :error
  def initialize(username = nil, password = nil)
     @username = username
     @password = password
     self.address_list = []
     @conn = Faraday.new(:url => base_api_url) do |builder|
      builder.request  :url_encoded
      builder.response :logger
      builder.adapter  :net_http
    end
  end

  def base_api_url
    'https://ws2.ondemand.qas.com'
  end

  def search(q)
    return unless q.length > 0
    self.address = q
    request_do_search
  end

  def request_do_search
    begin
      response = @conn.post do |req|
        req.url '/ProOnDemand/V3/ProOnDemandService.asmx'
        req.headers['Content-Type'] = 'text/xml; charset=utf-8'
        req.headers['SOAPAction'] = 'http://www.qas.com/OnDemand-2011-03/DoSearch'
        req.body = self.to_xml
      end
      puts response.body
      if response.status == 200 
        self.xml_result = response.body
        get_addresses
      else
        self.error = response.body
      end
    rescue Exception => e
      self.error = e.message
    end
  end

  def get_addresses
    # parse the xml twice as nokogiri
    doc = Nokogiri::XML(self.xml_result)
    soap_body = doc.xpath('//soap:Body').children[0].to_s
    doc2 = Nokogiri::XML(soap_body)
    address_list = []
    doc2.css('PicklistEntry').each do |a|
      address_list << {:moniker => a.at("Moniker").text, :partial_address => a.at("PartialAddress").text, :picklist => a.at("Picklist").text, :postcode => a.at("Postcode").text, :score => a.at("Score").text.to_i}
    end
    address_list
  end

  def to_xml
    xml = <<-XML_DOC
        <?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Header>
            <QAQueryHeader xmlns="http://www.qas.com/OnDemand-2011-03">
              <QAAuthentication>
                <Username>#{@username}</Username>
                <Password>#{@password}</Password>
              </QAAuthentication>
              <Security>xml</Security>
            </QAQueryHeader>
          </soap:Header>
          <soap:Body>
            <QASearch Localisation="AU" RequestTag="string" xmlns="http://www.qas.com/OnDemand-2011-03">
              <Country>AUS</Country>
              <Engine Flatten="true" Intensity="Extensive" PromptSet="Default" Threshold="10" Timeout="50" />
              <Layout>QADefault</Layout>
              <Search>#{address}</Search>
              <FormattedAddressInPicklist>true</FormattedAddressInPicklist>
            </QASearch>
          </soap:Body>
        </soap:Envelope>
      XML_DOC
      xml.strip
  end
end